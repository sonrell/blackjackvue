import VueRouter from 'vue-router'
import PlayScreen from "@/components/PlayScreen";
import StartingScreen from "@/components/StartingScreen";

const routes=[

    {
        path:"/startingscreen",
        component: StartingScreen
    },
    {

        path:"/play/",
        name: "play",
        component: PlayScreen
    }
]


const router= new VueRouter({routes})

export default router